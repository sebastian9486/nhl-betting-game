package com.aun.nhl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * @see http://websystique.com/spring-security/spring-security-4-hello-world-annotation-xml-example/
 * @author sebastian
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String ROLE_ADMIN = "ADMIN";
	private static final String ROLE_USER = "USER";

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("sebastian").password("start123").roles(ROLE_ADMIN, ROLE_USER);
		auth.inMemoryAuthentication().withUser("sadl").password("start123").roles(ROLE_USER);
		auth.inMemoryAuthentication().withUser("tobbi").password("start123").roles(ROLE_USER);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).invalidateHttpSession(true).logoutSuccessUrl("/login");

		http.authorizeRequests()
				// public logout page
				.antMatchers("/logout").permitAll()
				// public login page
				.antMatchers("/login").permitAll().antMatchers("/login.xhtml").permitAll()
				// all private pages
				.antMatchers("/**").access("hasRole('" + ROLE_ADMIN + "') or hasRole('" + ROLE_USER + "')")
				// custom login page
				.and().formLogin();
		// .and().formLogin().loginPage("/login.xhtml").loginProcessingUrl("/login").defaultSuccessUrl("/");
	}
}
