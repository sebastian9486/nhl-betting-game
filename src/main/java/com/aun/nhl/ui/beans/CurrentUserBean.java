package com.aun.nhl.ui.beans;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CurrentUserBean {

	public String getUsername() {
		User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return StringUtils.capitalize(user.getUsername());
	}

	public String getUsernameCapitalized() {
		return StringUtils.capitalize(getUsername());
	}
}
