package com.aun.nhl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		LOG.info("==============================================================================");
		LOG.info("\t Starting application");
		LOG.info("==============================================================================");

		SpringApplication.run(Application.class, args);

		LOG.info("==============================================================================");
		LOG.info("\t Application is up and running");
		LOG.info("==============================================================================");
	}

}
